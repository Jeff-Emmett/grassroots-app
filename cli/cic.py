#!/usr/bin/python3

import os
import sys
import argparse
import logging
import json
from getpass import getpass
from web3 import Web3

# TODO: optional verbose
logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger('main')

import config
from session import session
from eth_worker.eth_manager.exceptions import *
#from persistence import PersistenceInterface
from eth_worker.sql_persistence import PersistenceInterface
from wallet import UnsafeWallet, get_wallet_file

# contract_registry is found in SEMPO/eth_worker/eth_manager
# cannot use full include path because of side-effects in __init__.py
# among which importing sentry_sdk, sql_persistence which we don't need for blockchain-only manip
#from eth_interfaces.contract_registry import ContractRegistry
#from eth_interfaces import TransactionProcessor
from eth_worker.eth_manager.registry import ContractRegistry
from eth_worker.eth_manager.processor import TransactionProcessor

# the cmd modules handle individual subcommands
import cmds.cic.create as cmd_create
import cmds.cic.convert as cmd_convert
import cmds.cic.ls as cmd_ls

# parse command line arguments
# TODO: make flags contingent of subcommand
argparser = argparse.ArgumentParser(description='Minimal CLI stub for executing contract calls outside task manager using the SempoBlockchain repository as library')
argparser.add_argument('--bancor-dir', type=str, dest='bancor_dir', help='path to bancor git repository (default {})'.format(config.BANCOR_DIR))
argparser.add_argument('--sempo-dir', type=str, dest='sempo_dir', help='path to sempo app git repository (default {})'.format(config.SEMPO_DIR))
argparser.add_argument('-c', type=str, default=config.CONFIG_FILE, help='config file to use (default {})'.format(config.CONFIG_FILE))
argparser.add_argument('-v', help='be verbose', action='store_true')
argparser.add_argument('-vv', help='be more verbose', action='store_true')
sub = argparser.add_subparsers()
sub.required = False
sub.dest = 'command'
sub_create = sub.add_parser('create', help='create a token and add it to the CIC network')
cmd_create.process_args(sub_create)
sub_convert = sub.add_parser('convert', help='convert between tokens in the CIC network')
cmd_convert.process_args(sub_convert)
sub_ls = sub.add_parser('ls', help='list tokens in CIC network')

args = argparser.parse_args(sys.argv[1:])

subcmd = args.command
import cmds.cic.hello as cmd_mod
if subcmd == 'create':
    cmd_mod = cmd_create
elif subcmd == 'convert':
    cmd_mod = cmd_convert
elif subcmd == 'ls':
    cmd_mod = cmd_ls
else:
    subcmd = ''

if args.bancor_dir != None:
    config.BANCOR_DIR = args.bancor_dir
if args.sempo_dir != None:
    config.SEMPO_DIR = args.sempo_dir
if args.c != None:
    config.CONFIG_FILE = args.c
if args.v == True:
    logging.getLogger().setLevel(logging.INFO)
elif args.vv == True:
    logging.getLogger().setLevel(logging.DEBUG)


# process bancor directory
# bancor repo tag 0.5.16 seems to have forgotten to bump the version in package.json
# thus we will let 0.5.15 pass
# NOTE: deployed sempo bancor contracts were branched on aug 17th 2019, meaning they are using 0.4.8 or 0.4.9
try:
    f = open(config.BANCOR_DIR + "/package.json", 'r')
    s = json.load(f)
    f.close()
    if s['name'] != 'bancor-contracts' or s['version'] != '0.5.15':
    #    raise Exception("invalid bancor contract or wrong version")
        logg.warn('bancor contract version mismatch, have {}, want 0.4.16'.format(s['version']))
except:
    logg.warn('package.json in bancor repo not found')

# ordering is important here; contractregistry needs to be first, as the loop registering the contracts and abis uses the contractregistry itself for lookup
_bancor_abis = [
            'ContractRegistry', 
            'ERC20Token',
            'EtherToken',
            'SmartToken',
            'BancorNetwork',
            'BancorNetworkPathFinder',
            'BancorConverter',
            'BancorConverterRegistry',
            'BancorConverterFactory',
        ]


# process config
try:
    config.merge_from_file(config.CONFIG_FILE)
except:
    raise Exception("config read failed")



# main routine sets up components
# and muxes the subcommand
if __name__ == "__main__":

    s = session.Session(config)

    w = UnsafeWallet(s.w3, config.DEFAULT_ACCOUNT)
    if not w.isLoaded(config.DEFAULT_ACCOUNT) and not cmd_mod.is_read_only(args):
        wallet_file = get_wallet_file(config.WALLET_DIR, config.DEFAULT_ACCOUNT)
        pw = getpass('private key password: ')
        try :
            w.load(wallet_file, pw)
        except Exception as e:
            logg.critical("password mismatch {}".format(e))
            exit(1)

    logg.debug('wallet loaded: {}'.format(w.address))

    r = ContractRegistry(s.w3, config.BANCOR_DIR)
    contractregistry_abi = None
    with open('{}/solidity/build/contracts/ContractRegistry.json'.format(config.BANCOR_DIR), 'r') as abifile:
        j = json.load(abifile)
        if config.ETH_CONTRACT_CONTRACTREGISTRY_ADDRESS == None:
            address = j['networks'][str(config.ETH_CHAIN_ID)]['address']
            config.ETH_CONTRACT_CONTRACTREGISTRY_ADDRESS = Web3.toChecksumAddress(address)
        r.register_contract_by_name('ContractRegistry', config.ETH_CONTRACT_CONTRACTREGISTRY_ADDRESS, j['abi'])
        contractregistry_abi = j['abi']

    for k in _bancor_abis:
        abifilename = '{}/solidity/build/{}.abi'.format(config.BANCOR_DIR, k)
        with open(abifilename, 'r') as abifile:
            abi = abifile.read()
            abifile.close()
            contractname_hex = s.w3.toHex(text=k)
            
            logg.debug('get address {} {} {}'.format(contractname_hex, k, config.ETH_CONTRACT_CONTRACTREGISTRY_ADDRESS))
            address = r.get_contract_function(config.ETH_CONTRACT_CONTRACTREGISTRY_ADDRESS, 'addressOf')(k.encode('ascii')).call()
            r.register_contract_by_name(k, s.w3.toChecksumAddress(address), abi)


    p = PersistenceInterface(s.w3, w)
    t = TransactionProcessor(
        config.ETH_CHAIN_ID,
        s.w3,
        config.ETH_GAS_PRICE,
        config.ETH_GAS_LIMIT,
        p,
        r,
        )

    pathfinder_address = r.get_contract_address('BancorNetworkPathFinder')
    common_reserve_address = r.get_contract_function(pathfinder_address, 'anchorToken')().call()
    config.ETH_CONTRACT_ADDRESS = common_reserve_address
    abi = r.get_contract_abi('EtherToken')
    r.register_contract_by_name('EtherToken', common_reserve_address, abi)

    s.setWallet(w)
    s.setProcessor(t)
    s.setRegistry(r)
    s.setPersistence(p)
    s.setLogger(logg)

    cmd_mod.execute(s, args)
