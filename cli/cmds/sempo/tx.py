import logging

logg = logging.getLogger(__name__)

def process_args(argparser):
    argparser.add_argument('to_user_id', help='user id of recipient')
    argparser.add_argument('amount', help='amount of sarafu to send')


def validate_args(args):
    pass


def execute(client, config, eargs):
    r = client.get('/user/{}/?org={}'.format(eargs.to_user_id, eargs.o))
    transfer_accounts = r['data']['user']['transfer_accounts']
    transfer_account_count = len(transfer_accounts)
    if transfer_account_count > 1:
        logg.warning('user id {} has {} transfer accounts'.format(eargs.to_user_id, transfer_account_count))

    transfer_account_id = transfer_accounts[0]['id']
    logg.debug('have transfer account id {} for user id {}'.format(transfer_account_id, eargs.to_user_id))

    # NOTE: in the api "transfer_type" gets translated to db model column "transfer_subtype" when it has certain values
    data = {
        "recipient_transfer_account_id": transfer_account_id,
        "sender_transfer_account_id": None,
        "transfer_amount": int(eargs.amount),
        "target_balance": None,
        "transfer_type": "DISBURSEMENT",
        "org": eargs.o,
#        "token_id": 2,
            }
    r = client.post('/credit_transfer/', data)
    print(data)
    return True
