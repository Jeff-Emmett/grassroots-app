import logging

logg = logging.getLogger(__name__)

def process_args(argparser):
    argparser.add_argument('user_id', help='user id to edit')


def validate_args(args):
    pass


def execute(client, config, eargs):
    r = client.get('/kyc_application/?user_id={}'.format(eargs.user_id))
    kyc_id = r['data']['kyc_application']['id']
    logg.info('kyc {} found for user {}'.format(kyc_id, eargs.user_id))

    client.put('/kyc_application/{}/'.format(kyc_id), {
            "kyc_status":"VERIFIED",
        })
