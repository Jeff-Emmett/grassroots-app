def is_read_only(args=None):
    return True



def process_args(argparser):
    pass



def execute(session, eargs):
    config = session.config

    converter_registry_address = session.registry.get_contract_address('BancorConverterRegistry')
    tokens = session.registry.get_contract_function(converter_registry_address, 'getSmartTokens')().call()


    # query the token list
    smart_token_abi = session.registry.get_contract_abi('SmartToken')
    for token_address in tokens:
        session.registry.register_contract(token_address, smart_token_abi)
        token_name = session.registry.get_contract_function(token_address, 'name')().call()
        token_symbol = session.registry.get_contract_function(token_address, 'symbol')().call()
        print("{}\t{}\t{}".format(token_symbol, token_name, token_address))
