#!/bin/bash

d=$(realpath $(dirname ${BASH_SOURCE[0]}))
export PYTHONPATH=${d}:${d}/../src/sempo_extensions:${d}/../lib/python3.6
