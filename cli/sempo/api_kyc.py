def approve(client, user_id):
    if not isinstance(user_id, int):
        raise ValueError('user_id must be int')
  
    ep = '/kyc_application/'

    client.get('{}?user_id={}'.format(ep, user_id))
