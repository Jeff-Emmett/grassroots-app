#!/usr/bin/python

# Author: Louis Holbrook <dev@holbrook.no> (https://holbrook.no)
# License: GPLv3
# Description: interactive console for Sempo USSD session

# standard imports
import os
import sys
import uuid
import json
import argparse
import logging
import urllib
from urllib import request

# local imports
import config
from sempo.api_client import ApiClient

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger(__file__)


argparser = argparse.ArgumentParser(description='CLI tool to interface a Sempo USSD session')
argparser.add_argument('-c', type=str, default=config.CONFIG_FILE, help='config file to use (default {})'.format(config.CONFIG_FILE))
argparser.add_argument('--host', type=str, default='localhost')
argparser.add_argument('--port', type=int, default=9000)
argparser.add_argument('--nossl', help='do not use ssl (careful)', action='store_true')
argparser.add_argument('phone', help='phone number for USSD session')
argparser.add_argument('-v', help='be verbose', action='store_true')
argparser.add_argument('-vv', help='be more verbose', action='store_true')

args = argparser.parse_args(sys.argv[1:])

if args.c != None:
    config.CONFIG_FILE = args.c
if args.v == True:
    logging.getLogger().setLevel(logging.INFO)
elif args.vv == True:
    logging.getLogger().setLevel(logging.DEBUG)

# process config
try:
    config.merge_from_file(config.CONFIG_FILE)
except:
    raise Exception("config read failed")


if __name__ == "__main__":

    client = ApiClient(host=args.host, port=args.port, use_ssl=not args.nossl)
    url = client.api_url() + '/ussd/kenya?username={}&password={}'.format(config.USSD_USER, config.USSD_PASS)

    logg.info('service url {}'.format(url))
    logg.info('phone {}'.format(args.phone))

    session = uuid.uuid4().hex
    data = {
            'sessionId': session,
            'serviceCode': config.USSD_SERVICE_CODE,
            'phoneNumber': args.phone,
            'text': config.USSD_SERVICE_CODE,
        }

    state = "_BEGIN"
    while state != "END":

        if state != "_BEGIN":
            user_input = input('next> ')
            data['text'] = user_input

        req = urllib.request.Request(url)
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.add_header('Content-Type', 'application/json')
        req.data = data_bytes
        response = urllib.request.urlopen(req)
        response_data = response.read().decode('utf-8')
        state = response_data[:3]
        out = response_data[4:]
        print(out)
