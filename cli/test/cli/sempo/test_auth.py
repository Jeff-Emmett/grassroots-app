from sempo.api_client import ApiClient

# TODO: need fixture to set test data to test
def test_auth_correct(get_auth_credentials):
    c = ApiClient(
            host='localhost',
            port=9000,
            use_ssl=False,
            otp_secret='NYBHKSRMQCS2MV5I',
            )
    c.authorize(get_auth_credentials.email, get_auth_credentials.password)



# Tests that the client get method works as expected for methods that do not require authentication
def test_get_noauth():

    c = ApiClient(
            host='localhost',
            port=9000,
            use_ssl=False,
            )

    # Choose a get that doesn't require auth for simplicity
    c.get('/token/?symbols=SFU')



# Tests that the client get method works as expected for methods that require authentication
def test_get_auth(get_auth_credentials):
    c = ApiClient(
            host='localhost',
            port=9000,
            use_ssl=False,
            otp_secret='NYBHKSRMQCS2MV5I',
            )
    c.authorize(get_auth_credentials.email, get_auth_credentials.password)
    c.get('/me/')
