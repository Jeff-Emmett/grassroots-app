import os

from attrdict import AttrDict
import pytest

@pytest.fixture
def get_auth_credentials():
    return AttrDict({
            'email': os.environ['LOCAL_EMAIL'],
            'password': os.environ['LOCAL_PASSWORD']
        }) 
