#!/usr/bin/python

# Author: Louis Holbrook <dev@holbrook.no> (https://holbrook.no)
# License: GPLv3
# Description: cli interface for a subset of sempo platform functions

# standard imports
import sys
import argparse
import logging
import config

# local imports
from sempo.api_client import ApiClient
import cmds.sempo.kyc as cmd_kyc
import cmds.sempo.tx as cmd_tx
import cmds.sempo.reclaim as cmd_reclaim
import cmds.sempo.location as cmd_location

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger(__name__)


argparser = argparse.ArgumentParser(description='CLI tool to interface the Sempo API')
argparser.add_argument('-c', type=str, default=config.CONFIG_FILE, help='config file to use (default {})'.format(config.CONFIG_FILE))
argparser.add_argument('-o', type=int, default=config.CLIENT_DEFAULT_ORGANISATION, help='organisation id to query (default {})'.format(config.CLIENT_DEFAULT_ORGANISATION))
argparser.add_argument('--host', type=str, default='localhost')
argparser.add_argument('--port', type=int, default=9000)
argparser.add_argument('--nossl', help='do not use ssl (careful)', action='store_true')
argparser.add_argument('-v', help='be verbose', action='store_true')
argparser.add_argument('-vv', help='be more verbose', action='store_true')

sub = argparser.add_subparsers()
sub.dest = 'command'
sub_kyc = sub.add_parser('kyc', help='manipulate kyc applications')
cmd_kyc.process_args(sub_kyc)
sub_tx = sub.add_parser('tx', help='send sarafu to user')
cmd_tx.process_args(sub_tx)
sub_reclaim = sub.add_parser('reclaim', help='reclaim sarafu from user')
cmd_reclaim.process_args(sub_reclaim)
sub_location = sub.add_parser('location', help='set location for user')
cmd_location.process_args(sub_location)


args = argparser.parse_args(sys.argv[1:])

# TODO: add default command
cmd_mod = None
subcmd = args.command
if subcmd == 'kyc':
    cmd_mod = cmd_kyc
elif subcmd == 'tx':
    cmd_mod = cmd_tx
elif subcmd == 'reclaim':
    cmd_mod = cmd_reclaim
elif subcmd == 'location':
    cmd_mod = cmd_location

cmd_mod.validate_args(args)

if args.c != None:
    config.CONFIG_FILE = args.c
if args.v == True:
    logging.getLogger().setLevel(logging.INFO)
elif args.vv == True:
    logging.getLogger().setLevel(logging.DEBUG)

# process config
try:
    config.merge_from_file(config.CONFIG_FILE)
except:
    raise Exception("config read failed")


if __name__ == '__main__':
    otp_secret = config.CLIENT_OTP_SECRET
    client = ApiClient(host=args.host, port=args.port, use_ssl=not args.nossl, otp_secret=otp_secret)

    client.authorize(config.CLIENT_EMAIL, config.CLIENT_PASSWORD)
 
    cmd_mod.execute(client, config, args)
    #try:
    #    cmd_mod.execute(client, config, args)
    #except ValueError as e:
    #    logg.error('{}'.format(e))
    #    sys.exit(1)
