from web3 import Web3, HTTPProvider

class Session:
    """encapsulates all components needed for command execution
    """
    w3 = None
    processor = None
    persistence = None
    registry = None
    logger = None
    config = None
    wallet = None

    def __init__(self, config):
        self.config = config
        provider = HTTPProvider(config.ETH_HTTP_PROVIDER)
        self.w3 = Web3(provider)

    def setProcessor(self, processor):
        self.processor = processor

    def setPersistence(self, persistence):
        self.persistence = persistence

    def setRegistry(self, registry):
        self.registry = registry

    def setLogger(self, logger):
        self.logger = logger

    def setWallet(self, wallet):
        self.wallet = wallet
