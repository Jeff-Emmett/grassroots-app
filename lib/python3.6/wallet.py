from hx import remove_prefix
from os import listdir
from eth_keys import keys

class UnsafeWallet:


    def __init__(self, w3, path, address=None):
        self.w3 = w3
        self.private_key = None
        self.address = address
        self.loaded = False


    def load(self, filename, password):
        f = open(filename)
        pk_enc = f.read()
        f.close()
        pkhex = self.w3.eth.account.decrypt(pk_enc, password)
        pkbytes = bytes(pkhex)
        pk = keys.PrivateKey(pkbytes)
        self.private_key = pkhex
        self.address = pk.public_key.to_checksum_address() #self.w3.eth.accounts[0]

        self.loaded = True


    def isLoaded(self, account):
        return self.loaded
        #return self.w3.toChecksumAddress(account) in self.w3.eth.accounts


def wallet_file_address(address):
    return remove_prefix(address).lower()


def get_wallet_file(path, address):
    hx = wallet_file_address(address)
    for f in listdir(path): # todo truncate 0x prefix function
        if hx in f and f[:5] == 'UTC--':
            return '{}/{}'.format(path, f)
    return ValueError('no wallet file found for {} (path {})'.format(accounts, path))
