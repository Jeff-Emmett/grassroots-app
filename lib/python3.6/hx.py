import binascii

def remove_prefix(hx):
    if hx[:2] == '0x':
        return hx[2:]
