#!/bin/bash

# Author: Louis Holbrook <dev@holbrook.no> https://holbrook.no
# License: GPLv3
# Description: Creates required directory structure for app

d=`realpath $(dirname ${BASH_SOURCE[0]})`
d_log=$d/var/log/grassroots
d_lib=$d/var/lib/grassroots
d_run=$d/var/run/grassroots
mkdir -p $d_log
mkdir -p $d_lib
mkdir -p $d_run
