import config
import sys
import logging

from server import create_app
from server.utils.misc import decrypt_string

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

app = create_app()
with app.app_context():
    logg.debug('decrypting {}'.format(sys.argv[1]))
    print(decrypt_string(sys.argv[1]))
