import sys
import logging

logg = logging.getLogger()

from server import create_app, db
from server.models import User

app = create_app()

with app.app_context():
    user_id = sys.argv[1]
    u = db.session.query(User).execution_options(show_all=True).get(user_id) 
    if u == None:
        logg.error('user {} not found'.format(user_id))
        sys.exit(1)

    u.set_TFA_secret()
    db.session.add(u)
    db.session.commit()

    u = db.session.query(User).execution_options(show_all=True).get(user_id) 
    print(u.get_TFA_secret())
