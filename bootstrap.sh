#!/bin/bash

# Author: Louis Holbrook <dev@holbrook.no> (https://holbrook.no)
# License: GPLv3
# Description: Sets up virtualenv with the correct python interpreter and installs module dependencies for python and node
# 
# the dependencies in Debians are quirky; libmysqlclient-dev (mysql_config) and npm (node) are in conflict.
# to work around it use nvm to install node

# check tooling dependencies
which node
if [ $? -ne 0 ]; then
	2>&1 echo "node missing" && exit 1
fi

which npm
if [ $? -ne 0 ]; then
	2>&1 echo "npm missing" && exit 1
fi

which psql
if [ $? -ne 0 ]; then
	2>&1 echo "postgresql missing" && exit 1
fi

which redis-server
if [ $? -ne 0 ]; then
	2>&1 echo "redis missing" && exit 1
fi

which ganache-cli
if [ $? -ne 0 ]; then
	2>&1 echo "ganache-cli missing" && exit 1
fi

which netcat 
if [ $? -ne 0 ]; then
	2>&1 echo "netcat missing. you can still continue, but that's how I check open ports in the rest of the scripts. With it I will be able to inform you better"
fi


# prepare enviroment and set up file tree
wd=$(realpath $(dirname ${BASH_SOURCE[0]}))
pushd $wd
. quick_env.sh
. create_dirs.sh
sempodir=contrib/sempo
bancordir=contracts/bancor


# set up submodules
git submodule add --force https://github.com/grassrootseconomics/SempoBlockchain $sempodir
mkdir -vp contracts
git submodule add --force https://github.com/grassrootseconomics/contracts $bancordir
git submodule update --init
pushd contracts/bancor
git checkout $BANCOR_COMMIT
popd


# check whether python 3.6 is the system python
# breaks if 3.6 is among multiple 3.x systems are installed, and python3 does not point to 3.6
pyver='3.6'
py=''
pyconfd=''
pylibd="/usr/lib/python${pyver}"
if [ -d $pylibd ]; then
	pyconfd=$(find /usr/lib/python${pyver} -maxdepth 1 -type d  -name "config*" | head -n1)
	if [ -d $pyconfd ]; then
		py=$(which python3)
	fi
fi

# create temporary build dir
tmpd=$wd/.tmp
# if python is not in the path
# download it along with virtualenv and build
if [ -z "$py" ]; then

	mkdir -vp $tmpd
	pushd $tmpd

	pythonurl='https://www.python.org/ftp/python/3.6.10/Python-3.6.10.tgz'
	sigurl='https://www.python.org/ftp/python/3.6.10/Python-3.6.10.tgz.asc'
	pythonmd5='df5f494ef9fbb03a0264d1e9d406aada'
	pythonkey='0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D'
	pythonfile=$(basename $pythonurl)

	if [ ! -e "$pythonfile" ]; then
		curl -o $pythonfile $pythonurl 
	fi
	sigfile=$(basename $sigurl)
	if [ ! -e "$sigfile" ]; then
		curl -o $sigfile $sigurl 
	fi

	pythonmd5_actual=$(md5sum "$pythonfile" | awk '{ print $1; }')

	if [ "$pythonmd5" != "$pythonmd5_actual" ]; then
	       2>&1 echo "python targz md5 mismatch"
	       exit 1
	fi
	gpg --recv-keys $pythonkey #2> /dev/null
	gpg --verify $sigfile #2> /dev/null
	if [ $? -ne 0 ]; then
	      	2>&1 echo "python targz sig mismatch"
		exit 1
	fi

	tar -zxvf $pythonfile
	pushd 'Python-3.6.10'
	./configure --prefix=/usr
	make
	make install DESTDIR=$(pwd)/..
	popd
	py="$tmpd/usr/bin/python3.6"
	pyconfd="$tmpd/usr/lib/python3.6/config-3.6m-x86_64-linux-gnu"
	export PYTHONPATH="$tmpd/usr/lib/python3.6/site-packages/setuptools"

	# remove the build dir
	# rm -rf $tmpd

	popd
fi


# create the virtualenv
$py -m venv .venv
if [ $? -gt 0 ]; then
	>&2 echo "venv fail"
	exit 1
fi

# install missing config-3.6m files that virtualenv doesn't seem to put in
pyconfdv="config-${pyver}m"
mkdir -vp .venv/lib/python${pyver}/${pyconfdv}
install -vD $pyconfd/* .venv/lib/python${pyver}/${pyconfdv}/


# turn on virtualenv
source .venv/bin/activate


# recursive install dependencies for python for the local app wrapper
pip install wheel
pip install -r cli/requirements.txt
pip install -r src/sempo_extensions/requirements.txt


# recursive install dependencies for the sempo app itself
pushd $sempodir
pushd devtools
bash install_python_requirements.sh
popd
deactivate
pushd app
npm install
npm run-script build
popd
popd
