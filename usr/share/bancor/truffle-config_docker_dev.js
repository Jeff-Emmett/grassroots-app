let HDWalletProvider = require('@truffle/hdwallet-provider');

let mnemonic = 'history stumble mystery avoid embark arrive mom foil pledge keep grain dice';

// See <http://truffleframework.com/docs/advanced/configuration>
module.exports = {
    networks: {
       grassroots_docker_development: {
            host:       "app_ganache_1.app_backend",
            port:       7545,
            network_id: "*",         // Match any network id
            gasPrice:   20000000000, // Gas price used for deploys
            gas:        6721975      // Gas limit used for deploys
        },
    },
    mocha: {
        enableTimeouts: false,
        useColors:      true,
        bail:           true,
        reporter:       "list" // See <https://mochajs.org/#reporters>
    },
    solc: {
        optimizer: {
            enabled: true,
            runs:    200
        }
    },
    compilers: {
	solc: {
		version: "0.4.26"
	}
    }
};
