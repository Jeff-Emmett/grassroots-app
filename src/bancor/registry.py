import json
import logging

from web3 import Web3

import eth

logg = logging.getLogger(__name__)

# TODO: parse constants from ContractRegistryClient instead, OR use different contract which returns the constants as a list:
# ..nodes.constant = true
# ..nodes.nodeType = VariableDeclaration
# ..nodes.name = uppercase
# ..nodes.value.hexValue 
registry_key_names = [
        "ContractFeatures",
        "ContractRegistry",
        "BancorNetwork",
        "BancorFormula",
        "BancorConverterFactory",
        "BancorConverterUpgrader",
        "BancorConverterRegistry",
        "BancorConverterRegistryData",
        "BNTToken",
        "BancorX",
        "BancorXUpgrader",
        ]

class Registry:

    def __init__(self, w3, registry_address, bancor_dir):
        self.contracts = {}
        self.smart_tokens = {}
        self.reserve_tokens = {}

        with open('{}/solidity/build/ContractRegistry.abi'.format(bancor_dir)) as f:
            json_abi = json.load(f)
            f.close()
            self.contracts['ContractRegistry'] = w3.eth.contract(
                address=registry_address,
                abi=json_abi
                )
            logg.debug('loaded registry address {}'.format(self.contracts['ContractRegistry']))

        for k in registry_key_names:
            kHex = Web3.toHex(text=k)
            contract_address = self.contracts['ContractRegistry'].functions.addressOf(kHex).call()
            if contract_address == eth.zero_address:
                logg.debug('contract {} has zero-address, skipping'.format(contract_address))
                continue
            with open('{}/solidity/build/{}.abi'.format(bancor_dir, k)) as f:
                json_abi = json.load(f)
                f.close()
                self.contracts[k] = w3.eth.contract(
                        address=contract_address,
                        abi=json_abi
                )
                logg.info('loaded contract {} {}'.format(k, self.contracts[k].address))

        smarttoken_json_abi = None
        with open('{}/solidity/build/SmartToken.abi'.format(bancor_dir)) as f:
            smarttoken_json_abi = json.load(f)
            f.close()

        smarttoken_found = False
        for c in self.contracts['BancorConverterRegistry'].functions.getSmartTokens().call():
            logg.debug('smarttoken {}'.format(c))
            smart_token_contract = w3.eth.contract(
                address=c,
                abi=smarttoken_json_abi
            )
            smart_token_symbol = smart_token_contract.functions.symbol().call()
            self.smart_tokens[smart_token_symbol] = smart_token_contract
            logg.info('loaded smart token contract {} {}'.format(smart_token_symbol, smart_token_contract.address))

        reservetoken_json_abi = None
        with open('{}/solidity/build/EtherToken.abi'.format(bancor_dir)) as f:
            reservetoken_json_abi = json.load(f)
            f.close()
        for c in self.contracts['BancorConverterRegistry'].functions.getConvertibleTokens().call():
            logg.debug('reserve token {}'.format(c))
            reserve_token_contract = w3.eth.contract(
                address=c,
                abi=reservetoken_json_abi
            )
            reserve_token_symbol = reserve_token_contract.functions.symbol().call()
            self.reserve_tokens[reserve_token_symbol] = reserve_token_contract
            logg.info('loaded reserve token contract {} {}'.format(reserve_token_symbol, reserve_token_contract.address))


