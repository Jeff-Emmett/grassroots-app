#!flask/bin/python
import os
import sys
import logging
import traceback

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger(__name__)

os.environ["LOCATION"] = "LOCAL"

#from app.server import create_app
import server
from sempo_extensions.app.server.api.exchange_api import exchange_blueprint
from sempo_extensions.app.server.api.hello_api import hello_blueprint
from sempo_extensions.app.server.api.eth_api import eth_blueprint

current_app = server.create_app()

versioned_url = '/api/v1/ge'

logg.debug('in ge api override')
current_app.register_blueprint(exchange_blueprint, url_prefix=versioned_url)
current_app.register_blueprint(hello_blueprint, url_prefix=versioned_url)
current_app.register_blueprint(eth_blueprint, url_prefix=versioned_url)

logg.debug('path: {}'.format(sys.path))
current_app.run(debug=True, threaded=True, host='0.0.0.0', port=9000)
